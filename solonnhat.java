import java.util.Scanner;

public class solonnhat {

    public static int max3(int a, int b, int c) {
        if (a >= b && a >= c) {
            return a;
        }
        if(b >= c) {
            return b;
        }
        return Math.max(b, c);
    }

    public static void main(String[] args) {
        System.out.println("Nhap vao ba so:");
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();

        System.out.println( max3(a, b, c));
    }
}